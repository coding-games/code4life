import sys
import math
from typing import List

MOLECULES_TYPES = ['A', 'B', 'C', 'D', 'E']
NB_MOLECULES = 5

class NoRequiredMolecule(Exception):
    """ No more required molecule """
    pass
class NoCarriedSample(Exception):
    """ No carried sample by the robot """
    pass

class Sample:
    CARRIED_BY_ME = 0
    CARRIED_BY_OPPONENT = 1
    CARRIED_BY_CLOUD = -1

    def __init__(self, vals):
        self.id = int(vals[0])
        self.carried_by = int(vals[1])
        self.rank = int(vals[2])
        self.expertise_gain = vals[3]
        self.health = int(vals[4])
        self.costs = { MOLECULES_TYPES[i-5]: int(vals[i]) for i in range(5, 10) }
    
    def is_diagnosed(self):
        return self.health != -1
    
    def total_molecules(self):
        """ Returns the total number of molecules needed for this sample """
        return sum(self.costs.values())
    
    def __str__(self):
        s = "Sample#{} (${})\n".format(self.id, self.health)
        for cost in self.costs:
            tag = '!' if cost == self.expertise_gain else ''
            s += "{}x[{}{}] ".format(self.costs[cost], cost, tag)
        s+="\n"
        return s

    def __repr__(self):
        return self.__str__()
    
    def __hash__(self):
        return self.id
    
    def __eq__(self, other):
        return self.id == other.id
    
    def __ne__(self, other):
        return self.id != other.id



class Module:
    LOC_START = 'START_POS'
    LOC_SAMPLES = 'SAMPLES'
    LOC_DIAGNOSIS = 'DIAGNOSIS'
    LOC_MOLECULES = 'MOLECULES'
    LOC_LABORATORY = 'LABORATORY'


    def __init__(self, name):
        self.name = name
    
    def __str__(self):
        return self.name

    @staticmethod
    def cost_path(module_start, module_dest):
        if module_start.name == module_dest.name: return 0
        elif module_start.name == Module.LOC_START:
            return 2
        elif module_start.name == Module.LOC_SAMPLES:
            return 3
        elif module_start.name == Module.LOC_DIAGNOSIS:
            if module_dest.name == Module.LOC_LABORATORY: return 4
            else: return 3
        elif module_start.name == Module.LOC_MOLECULES:
            return 3
        elif module_start.name == Module.LOC_LABORATORY:
            if module_dest.name == Module.LOC_DIAGNOSIS: return 4
            else: return 3
        else:
            return 0 # error!
    
    @staticmethod
    def fromString(string: str):
        if string == Module.LOC_START: return START_AREA
        elif string == Module.LOC_SAMPLES: return MODULE_SAMPLES
        elif string == Module.LOC_DIAGNOSIS: return MODULE_DIAGNOSIS
        elif string == Module.LOC_MOLECULES: return MODULE_MOLECULES
        elif string == Module.LOC_LABORATORY: return MODULE_LABORATORY
        else: return None
    
START_AREA = Module(Module.LOC_START)
MODULE_SAMPLES = Module(Module.LOC_SAMPLES)
MODULE_DIAGNOSIS = Module(Module.LOC_DIAGNOSIS)
MODULE_MOLECULES = Module(Module.LOC_MOLECULES)
MODULE_LABORATORY = Module(Module.LOC_LABORATORY)

class Robot:
    def __init__(self, name):
        self.name = name
        self.carried_samples: List[Sample] = []
        self.moving = 0 # nb of turn to wait while moving
    
    def update(self, vals):
        self.location: Module = Module.fromString(vals[0])
        self.eta = int(vals[1])
        self.score = int(vals[2])
        self.storages = { MOLECULES_TYPES[i-3]: int(vals[i]) for i in range(3, 8) }
        self.expertises = { MOLECULES_TYPES[i-8]: int(vals[i]) for i in range(8, 13) }

    def costs_of(self, sample: Sample) -> dict:
        """ Calculates the real cost of all molecules types.
        The real cost is calculated in function of the expertise of the robot. """
        return { mol_type: self.real_cost(mol_type, sample.costs[mol_type]) for mol_type in MOLECULES_TYPES }
    
    def real_cost(self, mol_type, cost):
        """ Calculates the real cost of the molecule.
        The real cost is based on the expertise of the robot and the number of molecules already carried by the robot. """
        return cost - self.expertises[mol_type] - self.storages[mol_type]
    
    def total_real_cost(self, sample: Sample):
        """ Calculates the total of the real cost for each molecule of the specified sample """
        total = 0
        for mol_type in MOLECULES_TYPES:
            cost = self.real_cost(mol_type, sample.costs[mol_type])
            if cost > 0:
                total += cost
        return total
    
    def get_required_molecule(self, sample):
        """ Returns a missing molecule from the current carried_sample.
        This function takes into account the expertise of the robot.
        If the robot doesn't have a carried_sample, then this function raises NoCarriedSample.
        If the robot has all required molecules, then this function raises NoRequiredMolecule """
        if sample:
            for mol_type in MOLECULES_TYPES:
                if self.real_cost(mol_type, sample.costs[mol_type]) > 0:
                    return mol_type
            raise NoRequiredMolecule
        raise NoCarriedSample

    def can_produce_sample(self, sample) -> bool:
        """ Returns TRUE if the specified sample can be produces and FALSE otherwise """
        for mol_type in MOLECULES_TYPES:
            if self.real_cost(mol_type, sample.costs[mol_type]) > 0:
                return False
        return True
    
    def can_produce_all_samples(self) -> bool:
        """ Returns TRUE if the robot can produce all carried samples """
        for sample in self.carried_samples:
            if not self.can_produce_sample(sample):
                return False
        return True

    def produce_sample(self) -> Sample:
        """ Returns a sample that can be produced in the laboratory.
        If no sample can be produced, then None is returned """
        for sample in self.carried_samples:
            if self.can_produce_sample(sample):
                return sample
        return None
    
    def ordered_samples(self):
        return sorted(self.carried_samples, key= lambda s : s.health, reverse=True)

    def has_undiagnosed_sample(self):
        """ Returns TRUE if one of the 3 samples carried by the robot is undiagnosed """
        return any(not sample.is_diagnosed() for sample in self.carried_samples)
    
    def get_undiagnosed_sample(self):
        """ Returns one of the undiagnosed sample of the robot """
        return next(sample for sample in self.carried_samples if not sample.is_diagnosed())
    
    def total_expertise(self):
        """ Returns the total amount of expertise of the robot
        Sum of all expertises. """
        return sum(self.expertises.values())
    
    def nb_samples(self, rank):
        """ Returns the number of samples with the specified rank carried by this robot """
        count = 0
        for sample in self.carried_samples:
            if sample.rank == rank:
                count += 1
        return count

    def total_molecules(self):
        """ Returns the number of carried molecules """
        return sum(self.storages.values())

    def __str__(self):
        s = "[{}] at {}\n".format(self.name, self.location)
        for mol_type in MOLECULES_TYPES:
            s += "| {} ".format(mol_type)
        s+="|\n"
        for mol_type in MOLECULES_TYPES:
            s += "| {} ".format(self.storages[mol_type])
        s += "|\n"
        s += "Carried: " + str(self.carried_samples)
        return s

class Logic:
    def __init__(self, samples, robot, availables):
        self.samples = samples
        self.robot = robot
        self.availables = { MOLECULES_TYPES[i]: available for (i, available) in enumerate(availables) }
    
    def choose_molecule(self, sample):
        """ Chooses the molecule to take from the module MOLECULES """
        costs = self.robot.costs_of(sample)
        inputs = {}
        for mol_type in MOLECULES_TYPES:
            if costs[mol_type] > 0:
                inputs[mol_type] = (self.availables[mol_type] - costs[mol_type], self.availables[mol_type])

        return sorted(inputs, key= lambda key : inputs[key])[0]
           
    def best_sample_in_cloud(self):
        sample: Sample
        possible_matches = []
        for sample in self.samples.values():
            if sample.carried_by == Sample.CARRIED_BY_CLOUD and self.can_do_sample(sample):
                possible_matches.append(sample)
        matches = sorted(possible_matches, key= lambda s : s.health, reverse=True)
        if len(matches) > 0:
            return matches[0]
        else:
            return None

    def ranks_of_sample(self):
        """ Finds the rank of the sample to get """
        robot_total_expertise = self.robot.total_expertise()
        if robot_total_expertise < 3:
            return (3,0,0)
        elif robot_total_expertise < 6:
            return (2,1,0)
        elif robot_total_expertise < 10:
            return (1,1,1)
        else:
            return (0,1,2)
    
    def can_do_sample(self, sample: Sample):
        """ Checks if there is enough available molecules to do the sample.
        This function takes into account the expertise of the robot. """
        for mol_type in MOLECULES_TYPES:
            if robot.real_cost(mol_type, sample.costs[mol_type]) > self.availables[mol_type]:
                return False
        return True
    
    def get_impossible_sample(self) -> Sample:
        for sample in robot.carried_samples:
            if not self.can_do_sample(sample):
                return sample
        return None
    
    def get_doable_sample(self) -> Sample:
        for sample in robot.carried_samples:
            if self.can_do_sample(sample):
                return sample
        return None
    
    # ACTIONS
    def TAKE_MOLECULE(self, molecule_type):
        """ Takes a molecule from the MOLECULES module
        The robot must be on the MOLECULE module for this function to work. """
        print("CONNECT " + molecule_type)
    
    def PRODUCE_MEDICINE(self, sample: Sample):
        """ Produces the medicine from the LABORATORY module
        The robot must be on the LABORATORY module for this function to work. It must also have the right number of molecules to complete the medicine """
        print("CONNECT " + str(sample.id))
    
    def GOTO(self, module: Module):
        """ Moves the robot to the specified location """
        print("GOTO " + module.name)
        # Wait for moving
        self.robot.moving = Module.cost_path(self.robot.location, module) - 1
        print("Moving during {} turns".format(self.robot.moving), file=sys.stderr)

    def SAMPLE_RANK(self, rank):
        """ Transfers an undiagnosed sample data file of rank rank to the robot """
        print("CONNECT " + str(rank))
    
    def WAIT(self):
        """ Waits and do nothing """
        print("WAIT")
    
    def DIAGNOSE(self, sample):
        """ Diagnoses the sample """
        print("CONNECT " + str(sample.id))
    
    def DROP_TO_CLOUD(self, sample):
        """ Removes the sample from the robot to the cloud """
        print("CONNECT " + str(sample.id))

    def GRAB_FROM_CLOUD(self, sample):
        """ Removes the sample from the robot to the cloud """
        print("CONNECT " + str(sample.id))

# Bring data on patient samples from the diagnosis machine to the laboratory with enough molecules to produce medicine!

project_count = int(input())
for i in range(project_count):
    a, b, c, d, e = [int(j) for j in input().split()]



robot = Robot('BOB')
opponent = Robot('OPPONENT')
# game loop
while True:
    for i in range(2):
        if i == 0:
            robot.update(input().split())
        else:
            opponent.update(input().split())

    # DEBUG: describe robot
    print(robot, file=sys.stderr)
    print(opponent, file=sys.stderr)
    
    # Number of molecules available
    availables = [int(i) for i in input().split()]

    # Reset carried_samples
    robot.carried_samples = []
    samples = {}
    sample_count = int(input())
    # print("{} samples in the cloud:".format(sample_count), file=sys.stderr)
    for i in range(sample_count):
        sample = Sample(input().split())
        samples[sample.id] = sample
        # print(" - sample#{}".format(sample.id), file=sys.stderr)
        if sample.carried_by == Sample.CARRIED_BY_ME:
            # print("carried by me!", file=sys.stderr)
            robot.carried_samples.append(sample)
        
    # Create the logic engine
    logic = Logic(samples, robot, availables)

    if robot.moving > 0:
        logic.WAIT()
        robot.moving -= 1

    elif robot.location == START_AREA:
        logic.GOTO(MODULE_SAMPLES)
    
    elif robot.location == MODULE_SAMPLES:
        if len(robot.carried_samples) == 3:
            logic.GOTO(MODULE_DIAGNOSIS)
        else:
            ranks = logic.ranks_of_sample()
            for rank,nb in enumerate(ranks, 1):
                if robot.nb_samples(rank) < nb:
                    logic.SAMPLE_RANK(rank)
                    break

    elif robot.location == MODULE_DIAGNOSIS:
        if robot.has_undiagnosed_sample():
            s = robot.get_undiagnosed_sample()
            logic.DIAGNOSE(s)
        else:
            # Remove impossible samples
            impossible_sample = logic.get_impossible_sample()
            if impossible_sample:
                logic.DROP_TO_CLOUD(impossible_sample)
            # Grab new samples if dropped all
            elif len(robot.carried_samples) == 0:
                logic.GOTO(MODULE_SAMPLES)
            # Grab existing sample if any
            elif len(robot.carried_samples) < 3:
                best_sample = logic.best_sample_in_cloud()
                if best_sample:
                    logic.GRAB_FROM_CLOUD(best_sample)
                else:
                    if robot.can_produce_all_samples():
                        logic.GOTO(MODULE_LABORATORY)
                    else:
                        logic.GOTO(MODULE_MOLECULES)
            else:
                if robot.can_produce_all_samples():
                    logic.GOTO(MODULE_LABORATORY)
                else:
                    logic.GOTO(MODULE_MOLECULES)
            

    elif robot.location == MODULE_MOLECULES:
        try:
            # Find a sample doable
            ordered_samples = robot.ordered_samples()
            current_sample = None
            all_not_doable = True
            for sample in ordered_samples:
                if logic.can_do_sample(sample) and (robot.total_molecules() + robot.total_real_cost(sample) <= 10):
                    all_not_doable = False
                    if not robot.can_produce_sample(sample):
                        current_sample = sample
            if all_not_doable:
                # No doable sample: fetch new one
                logic.GOTO(MODULE_SAMPLES)
            elif not current_sample:
                logic.GOTO(MODULE_LABORATORY)
            else:
                required_molecule = logic.choose_molecule(current_sample)
                logic.TAKE_MOLECULE(required_molecule)
        except NoCarriedSample:
            # Goto diagnosis
            logic.GOTO(MODULE_DIAGNOSIS)
        except NoRequiredMolecule:
            # Goto laboratory
            logic.GOTO(MODULE_LABORATORY)


    elif robot.location == MODULE_LABORATORY:
        # Has a sample to produce
        sample_to_produce = robot.produce_sample()
        if not sample_to_produce:
            if len(robot.carried_samples) > 0:
                # Take next carried_sample
                logic.GOTO(MODULE_MOLECULES)
            else:
                logic.GOTO(MODULE_SAMPLES)
        else:
            # Use module
            logic.PRODUCE_MEDICINE(sample_to_produce)
